import React from "react";
import '../css/Footer.css';

class Footer extends React.Component{

    render() {

        return(
            <div className="footer">
                <div className="row">
                    <div className="col-sm-6">
                        <h5>Ressources</h5>
                        <li></li>
                        <li></li>
                        <li></li>
                    </div>
                    <div className="col-sm-6">
                        <h5>Informations</h5>
                        <li></li>
                        <li></li>
                        <li></li>
                    </div>
                </div>
            </div>
        )
    }
}

export default Footer;