import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import '../css/App.css';
import Navigation from "./Navigation";
import Accueil from "./Accueil";
import Test from "./Test";
import Menu from "./Menu";
import CDF from "./CDF";

//variable global
global.forfaitId = 0;
class App extends Component{
    render() {
        return (
            <div className="App">
                <Menu/>
                <Router>

                    <Navigation />
                    <Route path="/" exact component={Accueil}/>
                    <Route path="/test" exact component={Test}/>
                </Router>
            </div>
        );
    }
}

export default App;