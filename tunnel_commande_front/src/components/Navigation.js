import React from 'react';
import {Link} from 'react-router-dom';

function Navigation(){
    return(
        <ul style={{display: "flex"}}>
            <Link to="/">
                <li style={{margin:10, listStyle:"none"}}>Accueil</li>
            </Link>
            <Link to="/test">
                <li style={{margin:10, listStyle:"none"}}>Test</li>
            </Link>
            <Link to="/ChoixNumero">
                <li style={{margin:10, listStyle:"none"}}>CDF</li>
            </Link>
            <Link to="/forfait4">
                <li style={{margin:10, listStyle:"none"}}>Forfait 4</li>
            </Link>
        </ul>
    )
}

export default Navigation;