import '../css/Menu.css';
import React from 'react';
//import Forfait from "./Forfait";

class Menu extends React.Component{
    /*state ={
        items: [],
    }*/
    constructor(props) {
        super(props);
        this.state = {
            items: []
        }
        this.selected = this.selected.bind(this);
        this.test = this.test.bind(this);
    }

    test(e){
        global.forfaitId = e.target.dataset.id;
        alert(global.forfaitId)
    }

    selected(e){
        //btn Valider
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[2].classList.replace("enabled","disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[3].classList.replace("enabled","disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[4].classList.replace("enabled","disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[5].classList.replace("enabled","disabled")

        //e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[].classList.replace("disabled","enabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[parseInt(e.target.dataset.id) + 1].classList.replace("disabled","enabled")





        //console.log(e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[2].dataset.id.replace("test", e.target.dataset.id))
        //console.log(e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[2].dataset.id)

        //console.log(e.target.dataset.id) => les données passés en paramettre exemple: data-id="99"
        e.target.offsetParent.children[0].classList.remove("active")
        e.target.offsetParent.children[1].classList.remove("active")
        e.target.offsetParent.children[2].classList.remove("active")
        e.target.offsetParent.children[3].classList.remove("active")

        e.target.classList.toggle("active")

        //réinitialisation des classes
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[1].firstElementChild.firstElementChild.children[0].classList.replace("enabled", "disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[1].firstElementChild.firstElementChild.children[1].classList.replace("enabled", "disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[1].firstElementChild.firstElementChild.children[2].classList.replace("enabled", "disabled")
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[1].firstElementChild.firstElementChild.children[3].classList.replace("enabled", "disabled")

        //selection
        e.target.offsetParent.offsetParent.children[1].firstElementChild.firstElementChild.children[1].firstElementChild.firstElementChild.children[e.target.dataset.id - 1].classList.replace("disabled", "enabled")
    }


    componentDidMount() {
        fetch("http://localhost:8000/api/forfaits")
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({
                        isLoaded: true,
                        items: result
                    });
                },
                (error) => {
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            )
    }
    render() {
        const { items } = this.state;
        //console.log(items.length);
        return (
            <div>
                <div className="Menu">
                    <div className="container">
                        <div className="row navigation">
                            <div className="head-left-items col-sm-3 col-md-3">
                                Lorem ipsum
                            </div>
                            <div className="row head-right-items col-sm-9 col-md-9">

                                {items.map((item) => (
                                    <div
                                        className={"item-forfait col-sm-"+ 12/items.length + " col-md-"+12/items.length}
                                        //methods
                                        onClick={this.selected}
                                        //datas
                                        data-id={item.id}
                                        data-prix={item.prix}
                                        data-nom={item.nom}
                                    >
                                        {item.nom}<br/>{item.prix}€/mois
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                </div>
                <div className="detailForfait">
                    <div className="container">
                        <div className="row descriptionf">
                            {
                                items.map((forfait) => (
                                    <div
                                        className={"col-sm-6 disabled forfait" +forfait.id}
                                        data-id={forfait.id}
                                        data-prix={forfait.prix}
                                        data-nom={forfait.nom}
                                    >
                                        <h1>{forfait.nom}</h1>
                                        <div>
                                            <li><b>Standard vocal</b> personnalisé*</li>
                                            <li><b>{forfait.nb_heure} heures</b> de réception pendant 1 an</li>
                                            <li>Report des minutes**</li>
                                            <li>Redirection des appels vers un nombre de fixes et mobiles <b>ILLIMITÉ</b></li>
                                            <li>Bénéficiez de 3 mois offerts pour tout abonnement Annuel</li>
                                        </div>
                                    </div>
                                ))
                            }
                            <div className="col-sm-6 options">
                                <h1>Les Options</h1>
                                <form>
                                    <div className="custom-control custom-switch">
                                        <input type="checkbox" className="custom-control-input" id="customSwitch1" />
                                        <label className="custom-control-label" htmlFor="customSwitch1">
                                            Option 1 +1€/mois
                                        </label>
                                    </div>
                                    <div className="custom-control custom-switch">
                                        <input type="checkbox" className="custom-control-input" id="customSwitch2" />
                                        <label className="custom-control-label" htmlFor="customSwitch2">
                                            Option 2 +8€/mois
                                        </label>
                                    </div>
                                    <div className="custom-control custom-switch">
                                        <input type="checkbox" className="custom-control-input" id="customSwitch3" />
                                        <label className="custom-control-label" htmlFor="customSwitch3">
                                            Option 3 +5€/mois
                                        </label>
                                    </div>
                                    <div className="custom-control custom-switch">
                                        <input type="checkbox" className="custom-control-input" id="customSwitch4" />
                                        <label className="custom-control-label" htmlFor="customSwitch4">
                                            Option 4 +3€/mois
                                        </label>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                {
                    items.map((forfait) => (
                        <div className={"choisir disabled " + forfait.nom}>
                            <button
                                data-id={forfait.id}
                                onClick={this.test}
                                className="btn btnvalider btn-success">
                                Choisir mon numéro
                            </button>
                        </div>
                    ))
                }

            </div>
        );
    }
}

export default Menu;
