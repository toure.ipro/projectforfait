<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', ['uses' => 'ForfaitController@gestion', 'as' => 'forfait.gestion']);
//->middleware('auth')
Route::get("/forfait/{id}", ['uses' => 'ForfaitController@forfait', 'as' => 'forfait.affichage']);
Route::get("/forfait/{id}/creez-votre-standard", ['uses' => 'ForfaitController@creezvotrestandard', 'as' => 'forfait.creez-votre-standard']);
