<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('forfait')->insert([
            'nom' => "l'essentiel",
            'prix' => 21,
            'description' => "forfait 1",
        ]);
        DB::table('forfait')->insert([
            'nom' => "l'incontournable",
            'prix' => 22,
            'description' => "forfait 2",
        ]);
        DB::table('forfait')->insert([
            'nom' => "le Vaillant",
            'prix' => 23,
            'description' => "forfait 3",
        ]);
        DB::table('forfait')->insert([
            'nom' => "l'ultra",
            'prix' => 24,
            'description' => "forfait 4",
        ]);
        DB::table('forfait')->insert([
            'nom' => "Forfait Test",
            'prix' => 99,
            'description' => "forfait Test",
        ]);
        // \App\Models\User::factory(10)->create();
    }
}
