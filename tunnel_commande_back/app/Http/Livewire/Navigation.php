<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Navigation extends Component
{
    public $forfait1 = ["prix" => 21, "nom" => "l'essentiel"];
    public $forfait2 = ["prix" => 22, "nom" => "l'incontournable"];
    public $forfait3 = ["prix" => 23, "nom" => "le vaillant"];
    public $forfait4 = ["prix" => 24, "nom" => " l'ultra "];

    public $forfait = ["prix" => 21, "nom" => "l'essentiel"];

    public $classF1 ="selected";
    public $classF2 ="";
    public $classF3 ="";
    public $classF4 ="";

    public function selected1(){
        if($this->classF1 === ""){
            $this->classF1 = $this->classF1.'selected';
            $forfait['nom'] = $this->forfait1['nom'];
            $forfait['prix'] = $this->forfait1['prix'];
            $this->classF2 = "";
            $this->classF3 = "";
            $this->classF4 = "";
        }
        else{
            $this->classF1 = "selected";
        }
    }
    public function selected2(){
        if($this->classF2 === ""){
            $this->classF2 = $this->classF2.'selected';
            $forfait['nom'] = $this->forfait2['nom'];
            $forfait['prix'] = $this->forfait2['prix'];
            $this->classF1 = "";
            $this->classF3 = "";
            $this->classF4 = "";
        }
        else{
            $this->classF2 = "selected";
        }
    }
    public function selected3(){
        if($this->classF3 === ""){
            $this->classF3 = $this->classF3.'selected';
            $forfait['nom'] = $this->forfait3['nom'];
            $forfait['prix'] = $this->forfait3['prix'];
            $this->classF1 = "";
            $this->classF2 = "";
            $this->classF4 = "";
        }
        else{
            $this->classF3 = "selected";
        }
    }
    public function selected4(){
        if($this->classF4 === ""){
            $this->classF4 = $this->classF4.'selected';
            $forfait['nom'] = $this->forfait4['nom'];
            $forfait['prix'] = $this->forfait4['prix'];
            $this->classF2 = "";
            $this->classF3 = "";
            $this->classF1 = "";
        }
        else{
            $this->classF4 = "selected";
        }
    }


    public function render()
    {
        return view('livewire.navigation', [
            "forfait" => $this->forfait,
        ]);
    }
}
