<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Forfait;

class ForfaitController extends Controller
{
    public function gestion(){
        $lesForfaits = Forfait::All();
        return view("accueil/accueil")->with('lesForfaits', $lesForfaits);
    }

    public function forfait($id){
        $lesForfaits = Forfait::All();
        $unForfait = Forfait::where("id", $id)->first();

        return view('/forfait/forfait')->with(['lesForfaits' => $lesForfaits, 'unForfait' => $unForfait]);
    }

    public function creezvotrestandard($id){
        $lesForfaits = Forfait::All();
        $unForfait = Forfait::where("id", $id)->first();

        //CDF pour carte de france
        return view("/cdf/index")->with(['lesForfaits' => $lesForfaits, 'unForfait' => $unForfait]);
    }



    public function getAll(){
        return Forfait::all();
    }

    public function getForfait($id){
        return Forfait::where("id", $id)->first();
    }


}
