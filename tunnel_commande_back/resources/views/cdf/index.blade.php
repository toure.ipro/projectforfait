@extends('layout/layout')

@section('styles')
    <link rel="stylesheet" href="{{ asset('css/cdf.css') }}">
@endsection

@section('body')
    @foreach($lesForfaits as $forfait)
        @if($forfait->id === $unForfait->id)
            <li class="bg-red-400"><a href="/forfait/{{$forfait->id}}">{{$forfait->nom}} CDF</a></li>
        @else
            <li><a href="/forfait/{{$forfait->id}}">{{$forfait->nom}}</a></li>
        @endif
    @endforeach

    <h2 class="text-center">Choisissez votre numéro</h2>

    <!--affiche la carte de sélection de numéro-->
    <livewire:cartesvg/>



@endsection


