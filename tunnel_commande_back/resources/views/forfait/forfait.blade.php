@extends('layout/layout')

@section('body')
    @foreach($lesForfaits as $forfait)
        @if($forfait->id === $unForfait->id)
            <li class="bg-red-400"><a href="/forfait/{{$forfait->id}}">{{$forfait->nom}}</a></li>
        @else
            <li><a href="/forfait/{{$forfait->id}}">{{$forfait->nom}}</a></li>
        @endif
    @endforeach

    <a href="/forfait/{{$unForfait->id}}/creez-votre-standard" class="btn btn-success mt-8">Choisir mon numéro</a>
@endsection
